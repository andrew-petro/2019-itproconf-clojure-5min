# 2019-itproconf-clojure-5min

Resources supporting 2019 IT Professionals Conference (UW internal conference)
5 minute lightning talk about Clojure.

Short URL to this repo: <https://go.wisc.edu/r1u8m8>

Keynote deck <https://www.icloud.com/keynote/0ANk4rdDFZCcoTIHQ6iuPhbIQ#2019-apetro-itc-clojure-lightning>;
snapshot of that file
and of PDF and PowerPoint generated from are also in this repo.
