Andrew is a software developer in UW DoIT --> AIS --> WPS with 5 years 
experience working on [MyUW](https://it.wisc.edu/services/myuw) . Experience 
with Java and JavaScript make Clojure especially refreshing.

Andrew holds a B.S. in Computer Science and finds Clojure a fruitful context for
applying the data structures and algorithms studied those many years ago.

Book recommendations:  

+ [It doesn't have to be crazy at work](https://basecamp.com/books/calm)
+ [Living Clojure](http://shop.oreilly.com/product/0636920034292.do)
+ [Joy of Clojure](https://www.manning.com/books/the-joy-of-clojure-second-edition)

Pronouns: he/him.